<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
    <?php
        /**
         * Advantages and disadvantages of multiple inheritence    
         *Advantages:
         *  Dividing class into subclasses
         *  Subclasses will be able  to have properties and use methods from multiple super classes, or overide some if necessary
         *  Shorter code
         *  More organized code
         *  More flexible code
         **/
        trait Carnivore {
            private $foodCarnivore = "meat";
            public function getFoodCarnivore(){
                return $this -> foodCarnivore;
            }

            public function flag1(){
                echo "Carnivore trait's flag1 called";
                echo "\n";
            }

            public function flag2(){
                echo "Carnivore trait's flag2 called";
                echo "\n";
            }
        }

        trait Herbivore {
            private $foodHerbivore = "vegetables";
            public function getFoodHerbivore(){
                return $this -> foodHerbivore;
            }

            public function flag3(){
                echo "Herbivore's trait's flag3 called";
                echo "\n";
            }
        }

        class Omnivore{
            use Carnivore;
            use Herbivore;

            private $omnivore_food;

            public function __construct(){
                //we can use both functions from different classes
                $this -> omnivore_food  = ($this -> getFoodCarnivore())."+".($this -> getFoodHerbivore()); 
            }

            public function getOmnivoreFood(){
                return $this -> omnivore_food;
            }

            public function flag4(){
                echo "Omnivore flag4 is called";
                echo "\n";
            }

            public function flag5(){
                echo "Omnivore flag 5 is called";
                echo "\n";
            }
        }

        $person1 = new Omnivore();
        $person1 -> flag1();
        $person1 -> flag3();
        $person1 -> getOmnivoreFood();
        echo $person1-> getFoodCarnivore();
        echo "\n"; 
        echo $person1 -> getFoodHerbivore();
        echo "\n";
        echo $person1 -> getOmnivoreFood();
        echo "\n";
        $person1 -> flag2(); 
        
        /**
         * Disadvantages
         *  We can lose track of many super classes
         *  More complex
         *  Overriding of method with the same names exisiting in multiple super classes
         */

        trait Hi{
            public function greet(){
                echo "Hi";
                echo "\n";
            }
        }

        trait Hello{
            public function greet(){
                echo "Hello";
                echo "\n";
            }
        }

        class Person{
            use Hi;
            use Hello;
        }
        $person2 = new Person();
        $person2 -> greet(); // This will give a PhP fatal error since there are 2 functions with the same name : greet. (Collision)
    ?>
</body>
</html>